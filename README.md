                                                   **Customer form exercise v1**

A retail company is redoing the customer entry form on their website and has asked you to do the initial work. For MVP, the form should require the customer's name, email address, and zip code. It should follow UX best practices by including validation and showing helpful error messages to aid in completing the form. You won't have to worry about what happens with the data at this point, just showing a success message is sufficient.

- Requirements:

1. The application should display a form containing a name input (type=text), an email address input (type=email), a zip code input (type=number), and a button labelled Submit.
2. Given a user clicks the submit button and the name is less than 3 characters long, the error message Must be at least 3 characters appears near the name input and any values entered in an input are preserved.
3. Given a user clicks the submit button and the email address is not a valid email format of x@x.x (ex. john@email.com), the error message Must be a valid email appears near the email input and any values entered in an input are preserved.
4. Given a user clicks the submit button and the zip code is not a valid zip code format (ex. 12345), the error message Must be a valid zip code appears near the zip code input and any values entered in an input are preserved.
5. When a user submits a valid form, a message should appear to indicate that the form was successfully "submitted" and the input values are reset (note: the form should not actually submit).
6. Application should be broken down into multiple, logical components, stored in separate files.
7. Should include layout and component styling using CSS-in-JS or CSS modules or CSS libraries (ex. Bootstrap or Pure.css). (No fully styled 3rd party components)

- Stretch Requirements (These are optional requirementsfor additional practice):

1. Create a modal component that shows a success message when the user submits a valid form.front
