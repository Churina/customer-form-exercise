import React from 'react';
import { Form, Button } from 'semantic-ui-react';
import { useForm } from "react-hook-form";
import './App.css';
//import { toast } from 'react-toastify';

export default function FormValidation() {
  const [message, setMessage] = React.useState("")
  const { register, handleSubmit, formState: { errors }, reset} = useForm();
  const onSubmit = (data) => {
    console.log(data);
    setMessage("Successfully submitted!");
    //alert(JSON.stringify("Submitted"));
    reset();
  }
    return (
        <div className='form-container'>
            <Form  onSubmit={handleSubmit(onSubmit)}>
                <Form.Field>
                    <label>Name</label>
                    <input 
                      placeholder='Name' 
                      type="text"
                      {...register("name", {required: true, minLength: 3})} 
                     />
                </Form.Field>
                {errors.name && <p>Must be at least 3 characters</p>}

                <Form.Field>
                    <label>Email</label>
                    <input 
                      placeholder='Email' 
                      type="email"
                      {...register("email",
                      {required: true,
                        pattern: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i
                      })}                   
                     />
                </Form.Field>
                {errors.email && <p>Must be a valid email</p>}


                <Form.Field>
                    <label>Zip Code</label>
                    <input 
                      placeholder='Password' 
                      type="number"
                      {...register("zipCode", {required: true, 
                        pattern: /(^\d{5}$)|(^\d{5}-\d{4}$)/
                      })}
                    />
                </Form.Field>
                {errors.zipCode && <p>Must be a valid zip code</p>}

                <Button type='submit'>Submit</Button>
                <p>{message}</p>
            </Form>
          
        </div>
    )
}